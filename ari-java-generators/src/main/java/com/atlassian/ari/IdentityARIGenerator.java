package com.atlassian.ari;

import com.atlassian.ari.registry.IdentityARI;
import com.atlassian.ari.registry.ProductKey;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

public class IdentityARIGenerator extends Generator<ARI> {
    public IdentityARIGenerator() {
        super(ARI.class);
    }

    @Override
    public ARI generate(SourceOfRandomness random, GenerationStatus status) {
        String productId = random.choose(ProductKey.values()).getKey();
        return random.choose(new ARI[] {
                IdentityARI.allUsers(),
                IdentityARI.allPrincipals(),
                IdentityARI.productAdmin(productId),
                IdentityARI.productUser(productId)
        });
    }
}
