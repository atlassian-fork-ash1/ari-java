package com.atlassian.ari.registry;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public enum ProductKey {
    LEGACY_CONFLUENCE("confluence.ondemand"),
    LEGACY_JIRA_CORE("jira-core.ondemand"),
    LEGACY_JIRA_SERVICEDESK("jira-servicedesk.ondemand"),
    LEGACY_JIRA_SOFTWARE("jira-software.ondemand"),
    HIPCHAT("hipchat.cloud"),
    CONFLUENCE("confluence"),
    JIRA("jira"),
    JIRA_CORE("jira-core"),
    JIRA_SERVICEDESK("jira-servicedesk"),
    JIRA_SOFTWARE("jira-software"),
    BITBUCKET("bitbucket");

    private final static Map<String, ProductKey> BY_KEY_MAP = Arrays.stream(ProductKey.values())
            .collect(Collectors.toMap(ProductKey::getKey, e -> e));

    private final String key;

    ProductKey(@Nullable final String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return key;
    }

    @NotNull
    public String getKey() {
        return key;
    }

    @NotNull
    public static Optional<ProductKey> getByKey(@Nullable String key) {
        return Optional.ofNullable(BY_KEY_MAP.get(key));
    }
}
