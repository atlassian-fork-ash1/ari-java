package com.atlassian.ari.registry;

import com.atlassian.ari.ARI;
import com.atlassian.ari.ImmutableARI;
import org.jetbrains.annotations.NotNull;

public class IdentityARI {

    private static final ARI ALL_USERS = ARI.parse("ari:cloud:identity::users/");
    private static final ARI ALL_PRINCIPALS = ARI.parse("ari:cloud:identity::/");
    private static final ARI SITE_ADMIN_ROLE = ARI.parse("ari:cloud:identity::role/site/admin");

    private static final String ROLE_RESOURCE_TYPE = "role";
    private static final String PRODUCT_ADMIN_ID = "product/admin";
    private static final String PRODUCT_USER_ID = "product/user";

    @NotNull
    public static ARI allUsers() {
        return ALL_USERS;
    }

    @NotNull
    public static ARI allPrincipals() {
        return ALL_PRINCIPALS;
    }

    @NotNull
    public static ARI siteAdminRole() {
        return SITE_ADMIN_ROLE;
    }

    @NotNull
    public static ARI productAdmin(@NotNull String productId) {
        return ImmutableARI.builder()
                .resourceOwner(productId)
                .resourceType(ROLE_RESOURCE_TYPE)
                .resourceId(PRODUCT_ADMIN_ID)
                .build();
    }

    @NotNull
    public static ARI productUser(@NotNull String productId) {
        return ImmutableARI.builder()
                .resourceOwner(productId)
                .resourceType(ROLE_RESOURCE_TYPE)
                .resourceId(PRODUCT_USER_ID)
                .build();
    }
}
