package com.atlassian.ari;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ARIParserTest {
    @Rule
    public ExpectedException expectedExceptionRule = ExpectedException.none();

    @Test
    public void shouldParseValidARI() {
        final ARI ari = ARIParser.parse("ari:cloud:confluence:123:comment/1");
        assertThat(ari.getCloudId().get(), is("123"));
        assertThat(ari.getResourceOwner(), is("confluence"));
        assertThat(ari.getResourceType().get(), is("comment"));
        assertThat(ari.getResourceId().get(), is("1"));
    }

    @Test
    public void shouldParseARIWithoutCloudIdAndResourceId() {
        final ARI ari = ARIParser.parse("ari:cloud:identity::users/");
        assertThat(ari.getCloudId(), is(Optional.empty()));
        assertThat(ari.getResourceOwner(), is("identity"));
        assertThat(ari.getResourceType().get(), is("users"));
        assertThat(ari.getResourceId(), is(Optional.empty()));
    }

    @Test
    public void shouldParseARIWithoutCloudIdResourceIdAndResourceType() {
        final ARI ari = ARIParser.parse("ari:cloud:identity::/");
        assertThat(ari.getCloudId(), is(Optional.empty()));
        assertThat(ari.getResourceOwner(), is("identity"));
        assertThat(ari.getResourceType(), is(Optional.empty()));
        assertThat(ari.getResourceId(), is(Optional.empty()));
    }

    @Test
    public void shouldParseARIWithColonInResourceId() {
        ARI ari = ARIParser.parse("ari:cloud:identity::user/1234:abc-1234-def");
        assertThat(ari.value(), is("ari:cloud:identity::user/1234:abc-1234-def"));
        assertTrue(ari.getResourceId().isPresent());
        assertThat(ari.getResourceId().get(), is("1234:abc-1234-def"));
    }

    @Test
    public void shouldThrowForInvalidARI() {
        expectedExceptionRule.expect(IllegalArgumentException.class);
        ARIParser.parse("fakeAri");
    }

    @Test
    public void shouldThrowForInvalidARI2() {
        expectedExceptionRule.expect(IllegalArgumentException.class);
        ARIParser.parse("ari:cloud:bla");
    }

    @Test
    public void shouldReturnEmptyForInvalidARI() throws Exception {
        assertThat(ARIParser.tryParse("fakeAri"), is(Optional.empty()));
        assertThat(ARI.tryParse("fakeAri"), is(Optional.empty()));
    }
}