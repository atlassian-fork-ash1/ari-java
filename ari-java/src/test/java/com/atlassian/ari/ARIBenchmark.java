package com.atlassian.ari;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

public class ARIBenchmark {

    @State(Scope.Benchmark)
    public static class MyState {
        public ARI ari = ARI.parse("ari:cloud:confluence:123:comment/1");
    }

    @Benchmark
    public void measureToString(MyState state) {
        String s = state.ari.value();
    }
}
